<%@ include file="/jsp/common/includes.jsp"%>
<jsp:useBean id="dateObject" class="java.util.Date" />

<c:if test="${not empty departments}">
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<c:choose>
					<c:when test="${isHistory}">
						<th scoe="col"></th>
					</c:when>
					<c:otherwise>
						<th scoe="col"></th>
						<th scoe="col"></th>
						<th scoe="col"></th>
					</c:otherwise>
				</c:choose>
				<th scoe="col">Name</th>
				<th scoe="col">Beschreibung</th>
				<th scoe="col">Letzte �nderung von</th>
				<c:choose>
					<c:when test="${isHistory }">
						<th scoe="col">Revision angelegt am</th>
					</c:when>
					<c:otherwise>
						<th scoe="col">Letzte �nderung am</th>
					</c:otherwise>
				</c:choose>
				<th scoe="col">Mitarbeiter</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${departments}" var="department" varStatus="loo">
				<tr>
					<c:choose>
						<c:when test="${isHistory}">
							<c:url var="getPreviousVersionByIdAndRevisionNumber" value="getPreviousVersionByIdAndRevisionNumber">
								<c:param name="departmentId" value="${department.id}"></c:param>
								<c:param name="revisionNumber" value="${revisions[loo.index].id}"></c:param>
							</c:url>
							<td><a href="${getPreviousVersionByIdAndRevisionNumber}">Revisionsdetails</a></td>
						</c:when>
						<c:otherwise>
							<c:url var="editDepartment" value="edit">
								<c:param name="departmentId" value="${department.id}"></c:param>
							</c:url>
							<td><a href="${editDepartment}">Bearbeiten</a></td>
							<c:url var="deleteDepartment" value="delete">
							<c:param name="departmentId" value="${department.id}"></c:param>
						</c:url>
						<td><a href="${deleteDepartment}">L�schen</a></td>
						<c:url var="listPreviousRevisions" value="listPreviousRevisionsById">
							<c:param name="departmentId" value="${department.id}"></c:param>
						</c:url>
						<td><a href="${listPreviousRevisions}">Protokoll</a></td>
						</c:otherwise>
					</c:choose>
					<td>${department.name}</td>
					<td>${department.description}</td>
					
					<td>${department.updatedBy}</td>
					<c:choose>
						<c:when test="${isHistory }">
							<jsp:setproperty name="dateObject" property="time" value="${revisions[loo.index].timestamp}" />
							<td><fmt:formatDate value="${dateObject }" pattern="dd.MM.yyyy HH:mm:ss"/></td>
						</c:when>
						<c:otherwise>
							<td><fmt:formatDate value="${department.updatedTime }" pattern="dd.MM.yyyy HH:mm:ss"/></td>
						</c:otherwise>
					</c:choose>
					<td>${department.employees}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>