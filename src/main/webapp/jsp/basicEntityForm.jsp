<%@ include file="/jsp/common/includes.jsp"%>
<div class="container">
	<h2>${title}</h2>

	<form:form id="basicEntityForm" action="save?type=${type}" modelAttribute="basicEntity" method="post">
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<form:label path="name">Name:</form:label> <form:input path="name" class="form-control" id="lastname" name="lastname" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<form:label path="description">Description:</form:label> <form:input path="description" class="form-control" id="lastname" name="lastname" />
				</div>
			</div>
		</div>
		<form:input path="id" hidden="true" value="${basicEntity.id}"/>
		<form:input path="version" hidden="true" value="${basicEntity.version}"/>
	</form:form>
	<a class="btn btn-default" href="${pageContext.request.contextPath}/app/basicEntity/listAll?type=${type}" role="button">Cancel</a>
	<button type="submit" form="basicEntityForm" class="btn btn-default">Save</button>

</div>
