<%@ include file="/jsp/common/includes.jsp"%>
<div class="container">
	<div class="alert alert-success hidden" role="alert">Relations updated successfully</div>
	<div class="alert alert-danger hidden" role="alert">An error occurred while updating relations.Please contact your system administrator</div>
	<div id="oplqTable" class="table-responsive"></div>
	<button id="updateOplqOirRelations" type="submit" class="btn btn-dark">Update Relations</button>
</div>
<script src="${prototypeScriptsPath}/oplq.js"></script>