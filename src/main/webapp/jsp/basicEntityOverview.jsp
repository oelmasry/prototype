<%@ include file="/jsp/common/includes.jsp"%>
<div class="container">
	<h2>${title}</h2>

	<table class="table table-striped">
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th>Name</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${basicEntityList}" var="basicEntity" varStatus="loop">
				<tr>
					<c:url var="editBasicEntity" value="edit">
						<c:param name="basicEntityId" value="${basicEntity.id}"></c:param>
						<c:param name="type" value="${type}"></c:param>
					</c:url>
					<td><a href="${editBasicEntity}" class="glyphicon glyphicon-pencil"></a></td>
					
					<c:url var="deleteBasicEntity" value="delete">
						<c:param name="basicEntityId" value="${basicEntity.id}"></c:param>
						<c:param name="type" value="${type}"></c:param>
					</c:url>
					<td><a href="${deleteBasicEntity}" class="glyphicon glyphicon-remove"></a></td>
					<td>${basicEntity.name}</td>
					<td>${basicEntity.description}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<a class="btn btn-default" href="${pageContext.request.contextPath}/app/basicEntity/showForm?type=${type}" role="button">Create</a>
</div>