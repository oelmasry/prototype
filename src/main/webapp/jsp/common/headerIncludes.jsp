<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"
	import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ include file="/jsp/common/includes.jsp"%>
<%
	String lang = RequestContextUtils.getLocale(request).getLanguage();
	request.setAttribute("lang", lang);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

<link href="${cssPath}/jquery-ui-1.12.1.min.css" type="text/css" rel="stylesheet" />
<link href="${cssPath}/jquery-picklist.css" type="text/css" rel="stylesheet" />
<link href="${cssPath}/jquery-picklist-custom-style.css" type="text/css" rel="stylesheet" />
<link href="${cssPath}/colpick.css" type="text/css" rel="stylesheet" />
<link href="${cssPath}/custom-styles.css" type="text/css" rel="stylesheet" />
<link href="${bootstrapCssPath}/bootstrap.min.css" rel="stylesheet" />
<link href="${bootstrapSelectCssPath}/bootstrap-select.min.css" rel="stylesheet" />
<link href="${bootstrapCssPath}/datatables.min.css" type="text/css" rel="stylesheet" />

<!-- jQuery JavaScript -->
<script type="text/javascript" src="${scriptPath}/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="${scriptPath}/jquery-ui-1.12.1.min.js"></script>
<script type="text/javascript" src="${scriptPath}/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="${scriptPath}/jquery.highlight.js"></script>
<script type="text/javascript" src="${scriptPath}/jquery.mtz.monthpicker.js"></script>
<script type="text/javascript" src="${scriptPath}/datepicker-de.js"></script>
<script type="text/javascript" src="${scriptPath}/jquery-picklist.js"></script>
<script type="text/javascript" src="${scriptPath}/colpick.js"></script>
<script type="text/javascript" src="${scriptPath}/datatables.min.js"></script>
<script type="text/javascript" src="${scriptPath}/knockout-3.4.1.js"></script>
<script type="text/javascript" src="${bootstrapSelectScriptPath}/bootstrap-select.min.js"></script>


<script src="${editableGridScriptPath}/editablegrid.js"></script>
<script src="${editableGridScriptPath}/editablegrid_renderers.js"></script>
<script src="${editableGridScriptPath}/editablegrid_editors.js"></script>
<script src="${editableGridScriptPath}/editablegrid_validators.js"></script>
<script src="${editableGridScriptPath}/editablegrid_utils.js"></script>
<script src="${editableGridScriptPath}/editablegrid_charts.js"></script>
<script src="${editableGridScriptPath}/editablegrid_custom.js"></script>
<link rel="stylesheet" href="${editableGridCssPath}/editablegrid.css" type="text/css" media="screen">


