<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%-- <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%> --%>

<c:set var="imagePath" value="${pageContext.request.contextPath}/images"/>
<c:set var="cssPath" value="${pageContext.request.contextPath}/styles"/>
<c:set var="scriptPath" value="${pageContext.request.contextPath}/js/common"/>
<c:set var="bootstrapScriptPath" value="${pageContext.request.contextPath}/bootstrap/js"/>
<c:set var="bootstrapCssPath" value="${pageContext.request.contextPath}/bootstrap/css"/>
<c:set var="bootstrapSelectScriptPath" value="${pageContext.request.contextPath}/bootstrap-select/js"/>
<c:set var="bootstrapSelectCssPath" value="${pageContext.request.contextPath}/bootstrap-select/css"/>

<c:set var="editableGridScriptPath" value="${pageContext.request.contextPath}/editable-grid/js"/>
<c:set var="editableGridCssPath" value="${pageContext.request.contextPath}/editable-grid/css"/>


<c:set var="prototypeScriptsPath" value="${pageContext.request.contextPath}/js"/>

