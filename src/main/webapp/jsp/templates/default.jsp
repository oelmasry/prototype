<%@ include file="/jsp/common/includes.jsp"%>
<%@ page language="java"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title><spring:message code="prototype" /></title>
<%@ include file="/jsp/common/headerIncludes.jsp"%>
</head>
<body>
	<div id="wrapper">
<!-- 		<div id="header"> -->
<%-- 			<tiles:insertAttribute name="header" ignore="true" /> --%>
<!-- 		</div> -->
			<tiles:insertAttribute name="menu" ignore="true" />
		<div id="main">
			<div class="mainDiv">
				<tiles:insertAttribute name="body" />
			</div>
			<tiles:insertAttribute name="footer" ignore="true" />
		</div>
	</div>
</body>
</html>