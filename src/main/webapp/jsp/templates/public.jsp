<%@ include file="/jsp/common/includes.jsp"%>
<%@ page language="java"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
<head>
<title><spring:message code="prototype" /></title>
<%@ include file="/jsp/common/headerIncludes.jsp"%>
</head>
<body>
<div class="mainFlex">
	<div id="wrapper">
		<div id="header">
			<tiles:insertAttribute name="header" ignore="true" />
		</div>
		<div id="main">
			<tiles:insertAttribute name="body" ignore="true" />
		</div>
	</div>
	<tiles:insertAttribute name="footer" ignore="true" />
</div>
</body>
</html>