<%@ include file="/jsp/common/includes.jsp"%>
<%@page import="com.o.prototype.enums.BasicEntityType"%>
<c:set var="oplq" value="<%=BasicEntityType.OPLQ%>" />
<c:set var="oir" value="<%=BasicEntityType.OIR%>" />
<div>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="${pageContext.request.contextPath}/app/dashboard">Prototype</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Basic Data<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${pageContext.request.contextPath}/app/basicEntity/listAll?type=${oplq}">OPLQ</a></li>
						<li><a href="${pageContext.request.contextPath}/app/basicEntity/listAll?type=${oir}">OIR</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav">
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Relations<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${pageContext.request.contextPath}/app/oplq">OPLQ to OIR</a></li>
					</ul></li>
			</ul>
		</div>
	</nav>
</div>