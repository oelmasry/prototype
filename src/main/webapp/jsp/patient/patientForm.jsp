<%@ include file="/jsp/common/includes.jsp"%>

<div>





<div class="form-group col-md-1"> 
<label class="title_lable">Code</label>
<input class="form-control input-sm" disabled>
</div>    

<div class="form-group col-md-2"> 
<label class="title_lable">First Name</label>
<input class="form-control input-sm" data-bind="value:FirstName">
</div> 


<div class="form-group col-md-2"> 
<label class="title_lable">Middle Name</label>
<input class="form-control input-sm" data-bind="value:MiddleName">
</div>    

<div class="form-group col-md-2"> 
<label class="title_lable">Last Name</label>
<input class="form-control input-sm" data-bind="value:LastName">
</div>  

<div class="form-group col-md-2"> 
<label class="title_lable">Date Of Birth</label>
<input class="form-control input-sm datepicker" data-bind="value:DateOfBirth">
</div>  

<div class="form-group col-md-2"> 
<label class="title_lable">Phone No.</label>
<input class="form-control input-sm datepicker" data-bind="value:Phone">
</div>  

<div class="form-group col-md-2"> 
<label class="title_lable">Address</label>
<textarea class="form-control input-sm" data-bind="value:Address"></textarea>
</div>  

<div class="form-group col-md-2"> 
<label class="title_lable">Job</label>
<input class="form-control input-sm datepicker" data-bind="value:Job">
</div>  
   
<input type="button"  value="Save" data-bind="click:saveNewPatient">


  <div class="panel-group accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <table class="responsive-table">
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th scoe="col">Name</th>
                                            <th scoe="col">Cov. Type</th>
                                            <th scoe="col">Amount</th>
                                            <th scoe="col">Max Line</th>
                                            <th scoe="col">Max Claim</th>
                                            <th scoe="col">PreAuth</th>
                                            <th scoe="col">Actions</th>
                                        </tr>
                                    </thead>
                                 
                                    <tbody>
                                        <tr class="panel-heading meheade" role="tab" id="headingdgg">
                                            <td colspan="7">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent=".accordion" href="#collapsedgg" aria-expanded="true" aria-controls="collapsedgg" data-bind="text:key, attr:{ href:'#'+key() }"></a>
                                                </h4>
                                            </td>
                                            <td>
                                                <i data-toggle="modal" data-bind="click:AddRow" class="fa fa-plus"  style="color: #1f6a83;font-size: 15px;cursor: pointer;"></i>
                                                <i data-toggle="modal" data-target="#Delete" data-bind="click:DeleteFromDic" class="fa fa-trash" style="color: #1f6a83;font-size: 15px;cursor: pointer;"></i>
                                            </td>
                                        </tr>
                                    </tbody>
                                    
                                    
                                    <tbody data-bind="attr:{ id: key }" class="responsive-table panel-collapse collapse in panel-body" role="tabpaned" aria-labelledby="headingdgg">
                                       
                                        <tr data-bind="attr:{ id:'row'+ $parents[1].key() }">
                                            <td data-bind="text:$parent.locDesc"></td>
                                            <td data-title="Date" data-bind="text:moment(EffDate()).format('DD/MM/YYYY')"></td>
                                            <td data-title="Cov. Type" data-bind="text:Cov().CovTypeDesc"></td>
                                            <td data-title="Amount" data-bind="text:Cov().Amt_ratio"></td>
                                            <td data-title="Max Line" data-bind="text:Cov().MaxPerLine"></td>
                                            <td data-title="Max Claim" data-bind="text:Cov().MaxPerClaim"></td>
                                            <td data-title="PreAuth" data-bind="text:Cov().NeedPreAuth"></td>
                                            <td data-bind="click:DeleteDatedCov"><a href="#"><i class="fa fa-trash" style="font-size: 15px;cursor: pointer;" title="Delete"></i></a></td>
                                        </tr>
                                      
                                    </tbody>
                                 
                                </table>
                            </div>
                        </div>





</div>
<script src="${prototypeScriptsPath}/patient/patientForm.js"></script>