function getBasicObjectMetadata() {
	var metadata = [];
	metadata.push({
		name : "name",
		label : "Organisational Plain Language Questions (OPLQs)",
		datatype : "string",
		editable : true
	});
	return metadata;
}

function RowToBeUpdated(){
	this.row = null;
	this.oldValue = null;
	this.newValue = null;
	this.rowIndex = null;
	this.columnIndex = null;
}
