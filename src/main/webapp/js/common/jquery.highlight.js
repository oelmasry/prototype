/*
 * jQuery Highlight plugin
 * Copyright (c) 2009 Bartek Szopka
 * Licensed under MIT license.
 */

jQuery
		.extend({
			highlight : function(node, re, nodeName, className) {
				if (node.nodeType === 3) {
					var match = node.data.match(re);
					if (match) {
						var highlight = document
								.createElement(nodeName || 'em');
						highlight.className = className || 'highlight';
						var wordNode = node.splitText(match.index);
						wordNode.splitText(match[0].length);
						var wordClone = wordNode.cloneNode(true);
						highlight.appendChild(wordClone);
						wordNode.parentNode.replaceChild(highlight, wordNode);
						return 1;
					}
				} else if ((node.nodeType === 1 && node.childNodes)
						&& !/(script|style)/i.test(node.tagName)
						&& !(node.tagName === nodeName.toUpperCase() && node.className === className)) {
					for ( var i = 0; i < node.childNodes.length; i++) {
						i += jQuery.highlight(node.childNodes[i], re, nodeName,
								className);
					}
				}
				return 0;
			}
		});

jQuery.fn.unhighlight = function(options) {
	var settings = {
		className : 'highlight',
		element : 'em'
	};
	jQuery.extend(settings, options);

	return this.find(settings.element + "." + settings.className).each(
			function() {
				var parent = this.parentNode;
				if($(parent).html() !== undefined){
					$(parent).html($(parent).html().replace(/<em class="highlight">/g,'').replace(/<EM class=highlight>/g,'').replace(/<\/em>/g,'').replace(/<\/EM>/g,''));
				}
			}).end();
};

jQuery.fn.highlight = function(words, options) {
	var settings = {
		className : 'highlight',
		element : 'em',
		caseSensitive : false,
		wordsOnly : false
	};
	jQuery.extend(settings, options);

	if (words.constructor === String) {
		words = [ words ];
	}
	words = jQuery.grep(words, function(word, i) {
		return word != '';
	});
	words = jQuery.map(words, function(word, i) {
		return word.replace(/[-[\]{}()*+?.:,\\^$|#\s]/g, "\\$&");
	});
	if (words.length == 0) {
		return this;
	}
	;

	var flag = settings.caseSensitive ? "" : "i";
	var pattern = "(" + words.join("|") + ")";
	if (settings.wordsOnly) {
		pattern = "\\b" + pattern + "\\b";
	}
	var re = new RegExp(pattern, flag);

	return this.each(function() {
		jQuery.highlight(this, re, settings.element, settings.className);
	});
};