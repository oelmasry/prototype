window.onload = function() {
	var rowsToBeUpdated = [];

	function addToRowsToBeUpdated(editableGrid, rowIndex, columnIndex,
			oldValue, newValue, row) {
		var rowToBeUpdated = new RowToBeUpdated();
		rowToBeUpdated.oldValue = oldValue;
		rowToBeUpdated.newValue = newValue;
		rowToBeUpdated.rowIndex = rowIndex;
		rowToBeUpdated.rowId = editableGrid.getRowId(rowIndex);
		rowToBeUpdated.columnIndex = columnIndex;
		rowToBeUpdated.columnName = editableGrid.getColumnName(columnIndex);
		rowToBeUpdated.row = row;
		rowsToBeUpdated.push(rowToBeUpdated);
	}

	$('#updateOplqOirRelations').click(function(e) {
		var filteredRows = rowsToBeUpdated.filter(r => r.newValue !== r.oldValue);
		$.ajax({
            method: "POST",
            url: "/prototype/app/oplq/updateRelations",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(filteredRows),
            success: function (data) {
            	debugger;
            	rowsToBeUpdated = [];
            	editableGrid.fetchGrid();
            	$('.alert-success').removeClass('hidden');
            	document.documentElement.scrollTop = 0;
            },
            error: function (request) {
            	console.log("error in save");
            	rowsToBeUpdated = [];
            	editableGrid.fetchGrid();
            	$('.alert-success').removeClass('hidden');
            	document.documentElement.scrollTop = 0;
	     }
	   })

	});

	EditableGrid.prototype.fetchGrid = function() {
		var metadata = getBasicObjectMetadata();

		var dynamicColumns;
		$.ajax({
					type : "GET",
					contentType : "application/json",
					url : "/prototype/app/oplq/listAll",
					dataType : 'json',
					success : function(model) {
						if (model) {
							var data = [];
							data = model.data;
							dynamicColumns = model.dynamicColumns;
							var dynamicColumnNames = Object
									.keys(dynamicColumns);
							for (var i = 0; i < dynamicColumnNames.length; i++) {
								var columnName = dynamicColumnNames[i];
								var columnTitle = dynamicColumns[dynamicColumnNames[i]];
								var columnMetadata = {
									name : columnName,
									label : columnTitle,
									datatype : "boolean",
									editable : true
								};
								metadata.push(columnMetadata);
							}

							editableGrid.load({
								"metadata" : metadata,
								"data" : data
							});
							editableGrid.renderGrid("oplqTable",
									"table table-bordered");
						}
					},
					error : function() {
						console.log("error");
					}
				});

	};

	this.editableGrid = new EditableGrid("demo",
			{
				tableLoaded : function() {
					this.renderGrid("oplqTable", "table table-bordered");
				},
				modelChanged : function(rowIndex, columnIndex, oldValue,
						newValue, row) {
					addToRowsToBeUpdated(this, rowIndex, columnIndex, oldValue,
							newValue, row);
					console.log(rowsToBeUpdated);
				}
			});
	this.editableGrid.fetchGrid();
}