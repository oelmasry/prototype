package com.o.prototype.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.o.prototype.entities.BasicEntity;
import com.o.prototype.entities.OIR;
import com.o.prototype.entities.OPLQ;
import com.o.prototype.enums.BasicEntityType;
import com.o.prototype.models.EditableGridData;
import com.o.prototype.models.EditableGridModel;
import com.o.prototype.models.EditableGridRequest;
import com.o.prototype.models.EditableGridResponse;
import com.o.prototype.services.BasicEntityService;
import com.o.prototype.services.OPLQService;

@Controller
@RequestMapping(value = "oplq")
public class OPLQController {

	private static String DYNAMIC_COL_PREFIX = "col_";

	@Autowired
	private OPLQService oplqService;

	@Autowired
	private BasicEntityService basicEntityService;

	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "listAll")
	public EditableGridModel listAll() {
		List<OPLQ> oplqs = oplqService.listAll();
		List<BasicEntity> oirs = basicEntityService.listAll(BasicEntityType.OIR);
		Map<String, String> dynamicColumns = new HashMap<>();
		for (BasicEntity oir : oirs) {
			dynamicColumns.put(DYNAMIC_COL_PREFIX + String.valueOf(oir.getId()), oir.getName());
		}
		Set<Long> allOirIds = oirs.stream().map(o -> o.getId()).collect(Collectors.toSet());
		ObjectMapper m = new ObjectMapper();
		List<EditableGridData> data = new ArrayList<>();
		for (OPLQ oplq : oplqs) {
			Map<String, Object> oplqProperties = m.convertValue(oplq, Map.class);
			for (OIR oir : oplq.getOirs()) {
				oplqProperties.put(DYNAMIC_COL_PREFIX + String.valueOf(oir.getId()), allOirIds.contains(oir.getId()));
			}
			EditableGridData row = new EditableGridData();
			row.setId((Long) oplqProperties.get("id"));
			row.setValues(oplqProperties);
			data.add(row);
		}

		EditableGridModel model = new EditableGridModel();
		model.setDynamicColumns(dynamicColumns);
		model.setData(data);
		return model;
	}

	@ResponseBody
	@RequestMapping(value = "updateRelations", method = RequestMethod.POST)
	public EditableGridResponse updateRelations(@RequestBody List<EditableGridRequest> editableGridRequests) {
		Map<Long, List<Long>> oplqToOirsToBeAdded = new HashMap<>();
		Map<Long, List<Long>> oplqToOirsToBeRemoved = new HashMap<>();
		populateRelationUpdateMaps(editableGridRequests, oplqToOirsToBeAdded, oplqToOirsToBeRemoved);

		Map<Long, OPLQ> idToOplqMap = updateWithAddedOirs(oplqToOirsToBeAdded);
		updateWithRemovedOplqs(oplqToOirsToBeRemoved, idToOplqMap);

		oplqService.batchUpdate(new ArrayList<>(idToOplqMap.values()));

		return new EditableGridResponse(editableGridRequests.size());
	}

	private void populateRelationUpdateMaps(List<EditableGridRequest> editableGridRequests,
			Map<Long, List<Long>> oplqToOirsToBeAdded, Map<Long, List<Long>> oplqToOirsToBeRemoved) {
		for (EditableGridRequest model : editableGridRequests) {
			Long oplqId = Long.valueOf((Integer) model.getData().get("rowId"));
			Long oirId = Long.valueOf(((String) model.getData().get("columnName")).replace(DYNAMIC_COL_PREFIX, ""));
			Boolean newValue = (Boolean) model.getData().get("newValue");
			if (BooleanUtils.isTrue(newValue)) {
				List<Long> currentOirsToBeAdded = oplqToOirsToBeAdded.getOrDefault(oplqId, new ArrayList<>());
				currentOirsToBeAdded.add(oirId);
				oplqToOirsToBeAdded.put(oplqId, currentOirsToBeAdded);
			} else {
				List<Long> currentOirsToBeRemoved = oplqToOirsToBeRemoved.getOrDefault(oplqId, new ArrayList<>());
				currentOirsToBeRemoved.add(oirId);
				oplqToOirsToBeRemoved.put(oplqId, currentOirsToBeRemoved);
			}
		}
	}

	private void updateWithRemovedOplqs(Map<Long, List<Long>> oplqToOirsToBeRemoved, Map<Long, OPLQ> idToOplqMap) {
		for (Entry<Long, List<Long>> entry : oplqToOirsToBeRemoved.entrySet()) {
			Long oplqId = entry.getKey();
			List<Long> oirIds = entry.getValue();
			OPLQ oplq = idToOplqMap.getOrDefault(oplqId, oplqService.findById(oplqId));
			if (oplq == null) {
				continue;
			}
			for (Long oirId : oirIds) {
				OIR oir = (OIR) basicEntityService.findById(BasicEntityType.OIR, oirId);
				if (oir == null) {
					continue;
				}
				oplq.getOirs().removeIf(o -> o.getId().longValue() == oir.getId().longValue());
			}
			idToOplqMap.put(oplqId, oplq);
		}
	}

	private Map<Long, OPLQ> updateWithAddedOirs(Map<Long, List<Long>> oplqToOirsToBeAdded) {
		Map<Long, OPLQ> idToOplqMap = new HashMap<>();
		for (Entry<Long, List<Long>> entry : oplqToOirsToBeAdded.entrySet()) {
			Long oplqId = entry.getKey();
			List<Long> oirIds = entry.getValue();
			OPLQ oplq = oplqService.findById(oplqId);
			if (oplq == null) {
				continue;
			}
			for (Long oirId : oirIds) {
				OIR oir = (OIR) basicEntityService.findById(BasicEntityType.OIR, oirId);
				if (oir == null) {
					continue;
				}
				oplq.getOirs().add((OIR) oir);
			}
			idToOplqMap.put(oplqId, oplq);
		}
		return idToOplqMap;
	}

}
