package com.o.prototype.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.o.prototype.entities.BasicEntity;
import com.o.prototype.enums.BasicEntityType;
import com.o.prototype.factory.BasicEntityFactory;
import com.o.prototype.services.BasicEntityService;

@Controller
@RequestMapping("basicEntity")
@SessionAttributes("basicEntity")
public class BasicEntityController {

	private static final String BASIC_ENTITY_ID = "basicEntityId";
	private static final String BASIC_ENTITY = "basicEntity";
	@Autowired
	BasicEntityFactory basicEntityFactory;

	@Autowired
	BasicEntityService basicEntityService;

	private Logger logger = Logger.getLogger(this.getClass());

	@RequestMapping(value = "showForm")
	public ModelAndView showForm(@RequestParam(name = "type") BasicEntityType type) {
		return prepareAndGetFormModelAndView(type, null);
	}

	@RequestMapping(value = "listAll")
	public ModelAndView listAll(@RequestParam(name = "type") BasicEntityType type) {
		return prepareAndGetOverviewModelAndView(type);
	}

	private ModelAndView prepareAndGetFormModelAndView(BasicEntityType type, BasicEntity basicEntity) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("basicEntityForm");
		mav.addObject(BASIC_ENTITY, basicEntity == null ? basicEntityFactory.create(type) : basicEntity);
		mav.addObject("title", basicEntityFactory.getTitle(type));
		mav.addObject("type", type);
		return mav;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute(BASIC_ENTITY) BasicEntity basicEntity) {
		ModelAndView mav;
		BasicEntityType type = BasicEntityType.valueOf((String) request.getParameter("type"));
		try {
			if (basicEntity.getId() == null) {
				basicEntityService.create(type, basicEntity);
			} else {
				basicEntityService.update(type, basicEntity);
			}
		} catch (Exception e) {
			logger.error("Error in save", e);
		} finally {
			mav = prepareAndGetOverviewModelAndView(type);
		}
		return mav;
	}

	private ModelAndView prepareAndGetOverviewModelAndView(BasicEntityType type) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("basicEntityOverview");
		mav.addObject("basicEntityList", basicEntityService.listAll(type));
		mav.addObject("title", basicEntityFactory.getTitle(type));
		mav.addObject("type", type);

		return mav;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = BASIC_ENTITY_ID) Long basicEntityId) {
		BasicEntityType type = BasicEntityType.valueOf((String) request.getParameter("type"));
		BasicEntity basicEntity = basicEntityService.findById(type, basicEntityId);
		return prepareAndGetFormModelAndView(type, basicEntity);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = BASIC_ENTITY_ID) Long basicEntityId) {
		BasicEntityType type = BasicEntityType.valueOf((String) request.getParameter("type"));
		basicEntityService.deleteById(type, basicEntityId);
		return prepareAndGetOverviewModelAndView(type);
	}

}
