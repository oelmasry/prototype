package com.o.prototype.listeners;

import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.flywaydb.core.Flyway;

public class FlywayMigrationServletContextListener implements ServletContextListener {

	private Logger logger = Logger.getLogger(this.getClass());
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {
			Properties props = new Properties();
			props.load(
					sce.getServletContext().getResourceAsStream("/WEB-INF/conf/flyway.properties"));
			Flyway flyway = Flyway.configure().configuration(props).load();
			int successfulMigrations = flyway.migrate();
			logger.info(successfulMigrations + " successful migrations applied");
		} catch (Exception e) {
			logger.error("Error in flyway migration", e);
		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

}
