package com.o.prototype.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "OPLQ")
public class OPLQ extends BasicEntity {

	private static final long serialVersionUID = -7810170193679775182L;

	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@JoinTable(name = "OPLQ_OIR", joinColumns = { @JoinColumn(name = "OPLQ_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "OIR_ID") })
	Set<OIR> oirs = new HashSet<>();

	public Set<OIR> getOirs() {
		return oirs;
	}

	public void setOirs(Set<OIR> oirs) {
		this.oirs = oirs;
	}

}
