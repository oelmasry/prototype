package com.o.prototype.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.o.prototype.dao.basic.BasicEntityDAO;
import com.o.prototype.dao.basic.OIRBasicDAO;
import com.o.prototype.dao.basic.OPLQBasicDAO;
import com.o.prototype.entities.BasicEntity;
import com.o.prototype.entities.OIR;
import com.o.prototype.entities.OPLQ;
import com.o.prototype.enums.BasicEntityType;

@Component
public class BasicEntityFactory {

	@Autowired
	private OIRBasicDAO oirDAO;

	@Autowired
	private OPLQBasicDAO oplqDAO;

	public BasicEntity create(BasicEntityType type) {
		switch (type) {
		case OIR:
			return new OIR();
		case OPLQ:
			return new OPLQ();
		default:
			return null;
		}
	}

	public BasicEntityDAO getDao(BasicEntityType basicLookupTyp) {
		switch (basicLookupTyp) {
		case OIR:
			return oirDAO;
		case OPLQ:
			return oplqDAO;
		default:
			return null;
		}
	}

	public String getTitle(BasicEntityType basicLookupTyp) {
		switch (basicLookupTyp) {
		case OIR:
			return "Organisation Information Requirements (OIRs)";
		case OPLQ:
			return "Organisational Plain Language Questions (OPLQs)";
		default:
			return null;
		}
	}

}
