package com.o.prototype.services;

import java.util.List;

import com.o.prototype.entities.OPLQ;

public interface OPLQService {

	public List<OPLQ> listAll();

	public OPLQ findById(Long id);

	public List<OPLQ> batchUpdate(List<OPLQ> updatedOplqs);

}
