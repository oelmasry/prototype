package com.o.prototype.services;

import java.util.List;

import com.o.prototype.entities.OIR;

public interface OIRService {

	public List<OIR> listAll();

	public OIR findById(Long id);

}
