package com.o.prototype.services;

import java.util.List;

import com.o.prototype.entities.BasicEntity;
import com.o.prototype.enums.BasicEntityType;

public interface BasicEntityService {

	public List<BasicEntity> listAll(BasicEntityType type);

	public BasicEntity findById(BasicEntityType type, Long id);

	public void create(BasicEntityType type, BasicEntity entity);

	public BasicEntity update(BasicEntityType type, BasicEntity entity);

	public void deleteById(BasicEntityType type, Long entityId);
}
