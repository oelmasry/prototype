package com.o.prototype.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.o.prototype.dao.OPLQDAO;
import com.o.prototype.entities.OPLQ;
import com.o.prototype.services.OPLQService;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class OPLQServiceImpl implements OPLQService {

	@Autowired
	private OPLQDAO oplqDAO;

	@Override
	public List<OPLQ> listAll() {
		return oplqDAO.listAll();
	}

	@Override
	public OPLQ findById(Long id) {
		return oplqDAO.findById(id);
	}

	@Transactional(readOnly = false)
	@Override
	public List<OPLQ> batchUpdate(List<OPLQ> updatedOplqs) {
		return oplqDAO.batchUpdate(updatedOplqs);

	}

}
