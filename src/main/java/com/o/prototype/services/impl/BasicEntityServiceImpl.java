package com.o.prototype.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.o.prototype.entities.BasicEntity;
import com.o.prototype.enums.BasicEntityType;
import com.o.prototype.factory.BasicEntityFactory;
import com.o.prototype.services.BasicEntityService;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class BasicEntityServiceImpl implements BasicEntityService {

	@Autowired
	BasicEntityFactory basicEntityFactory;

	@Override
	public List<BasicEntity> listAll(BasicEntityType type) {
		return basicEntityFactory.getDao(type).listAll();
	}

	@Override
	public BasicEntity findById(BasicEntityType type, Long id) {
		return basicEntityFactory.getDao(type).findById(id);
	}

	@Transactional(readOnly = false)
	@Override
	public void create(BasicEntityType type, BasicEntity entity) {
		basicEntityFactory.getDao(type).create(entity);

	}

	@Transactional(readOnly = false)
	@Override
	public BasicEntity update(BasicEntityType type, BasicEntity entity) {
		return basicEntityFactory.getDao(type).update(entity);
	}

	@Transactional(readOnly = false)
	@Override
	public void deleteById(BasicEntityType type, Long entityId) {
		basicEntityFactory.getDao(type).deleteById(entityId);
	}

}
