package com.o.prototype.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.o.prototype.dao.OIRDAO;
import com.o.prototype.entities.OIR;
import com.o.prototype.services.OIRService;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class OIRServiceImpl implements OIRService {

	@Autowired
	private OIRDAO oirDAO;

	@Override
	public List<OIR> listAll() {
		return oirDAO.listAll();
	}

	@Override
	public OIR findById(Long id) {
		return oirDAO.findById(id);
	}


}
