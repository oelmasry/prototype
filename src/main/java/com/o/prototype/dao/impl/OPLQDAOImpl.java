package com.o.prototype.dao.impl;

import org.springframework.stereotype.Repository;

import com.o.prototype.dao.OPLQDAO;
import com.o.prototype.entities.OPLQ;

@Repository
public class OPLQDAOImpl extends AbstractDAOImpl<OPLQ> implements OPLQDAO {

	public OPLQDAOImpl() {
		super(OPLQ.class);
	}

}
