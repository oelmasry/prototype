package com.o.prototype.dao.impl;

import org.springframework.stereotype.Repository;

import com.o.prototype.dao.OIRDAO;
import com.o.prototype.entities.OIR;

@Repository
public class OIRDAOImpl extends AbstractDAOImpl<OIR> implements OIRDAO {

	public OIRDAOImpl() {
		super(OIR.class);
	}

}
