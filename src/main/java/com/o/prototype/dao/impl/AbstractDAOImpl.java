/**
 * 
 */
package com.o.prototype.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.o.prototype.dao.AbstractDAO;
import com.o.prototype.entities.AbstractEntity;

@Repository
public abstract class AbstractDAOImpl<T extends AbstractEntity> implements AbstractDAO<T> {

	protected EntityManager entityManager;

	private Class<T> type;

	private Logger logger = Logger.getLogger(AbstractDAOImpl.class);

	public AbstractDAOImpl(Class<T> type) {
		super();
		this.type = type;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext(unitName = "basicPersistence")
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	protected void setType(Class type) {
		this.type = type;
	}

	@Override
	public List<T> listAll() {
		TypedQuery<T> q = entityManager.createQuery("from " + type.getName(), type);
		return q.getResultList();
	}

	@Override
	public void create(final T entity) {
		entityManager.persist(entity);
		logger.debug("Entity of type: " + entity.getClass().getSimpleName() + " with id: " + entity.getId()
				+ " and version: " + entity.getVersion() + " is saved correclty", null);
	}

	@Override
	public T update(final T entity) {
		if (entity.getId() != null) {
			T foundEntity = entityManager.find(type, entity.getId());
			if (foundEntity == null) {
				// Updating a deleted entity is not handled automatically by the
				// hibernate otimistic locking mechanism
				throw new OptimisticLockException("Entity is deleted in another transaction.");
			}
		}
		T t = entityManager.merge(entity);
		// entityManager.lock(t, LockModeType.oTIMISTIC_FORCE_INCREMENT);
		logger.debug("Entity of type: " + entity.getClass().getSimpleName() + " with id: " + entity.getId()
				+ " and version: " + entity.getVersion() + " is updated correclty", null);
		return t;
	}

	@Override
	public List<T> batchUpdate(final List<T> entities) {
		int i = 0;
		List<T> updated = new ArrayList<>();
		String className = "";
		for (T entity : entities) {
			className = entities.get(0).getClass().getSimpleName();

			if (entity.getId() != null) {
				T foundEntity = entityManager.find(type, entity.getId());
				if (foundEntity == null) {
					// Updating a deleted entity is not handled automatically by the
					// hibernate optimistic locking mechanism
					throw new OptimisticLockException("Entity is deleted in another transaction.");
				}
			}

			T t = entityManager.merge(entity);
			entityManager.lock(t, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
			updated.add(t);
			i++;
			if (i % 20 == 0) {
				entityManager.flush();
				entityManager.clear();
				logger.debug("batchUpdate " + className + ": Batch of entities is flushed. Current index: " + i, null);
			}
		}
		logger.debug(updated.size() + " " + className + " are updated.", null);
		return updated;
	}

	@Override
	public void deleteById(Long entityId) {
		T foundEntity = findById(entityId);
		if (foundEntity == null) {
			// Deleting a deleted entity is not handled automatically by the
			// hibernate otimistic locking mechanism
			throw new OptimisticLockException("Entity is deleted in another transaction.");
		}

		T t = entityManager.merge(foundEntity);
		entityManager.remove(t);
		logger.debug("Entity of type: " + foundEntity.getClass().getSimpleName() + " with id: " + foundEntity.getId()
				+ " and version: " + foundEntity.getVersion() + " is deleted correclty", null);
	}

	@Override
	public T findById(Long id) {
		T object = entityManager.find(type, id);
		if (object == null) {
			logger.debug(type.getSimpleName() + " not found", null);
		}
		return object;
	}

}
