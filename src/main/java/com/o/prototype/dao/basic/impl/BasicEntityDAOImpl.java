package com.o.prototype.dao.basic.impl;

import com.o.prototype.dao.basic.BasicEntityDAO;
import com.o.prototype.dao.impl.AbstractDAOImpl;
import com.o.prototype.entities.BasicEntity;

public class BasicEntityDAOImpl extends AbstractDAOImpl<BasicEntity> implements BasicEntityDAO {

	public BasicEntityDAOImpl() {
		super(BasicEntity.class);
	}


}
