package com.o.prototype.dao.basic;

import com.o.prototype.dao.AbstractDAO;
import com.o.prototype.entities.BasicEntity;

public interface BasicEntityDAO extends AbstractDAO<BasicEntity> {

}
