package com.o.prototype.dao.basic.impl;

import org.springframework.stereotype.Repository;

import com.o.prototype.dao.basic.OPLQBasicDAO;
import com.o.prototype.entities.OPLQ;

@Repository
public class OPLQBasicDAOImpl extends BasicEntityDAOImpl implements OPLQBasicDAO {

	public OPLQBasicDAOImpl() {
		setType(OPLQ.class);
	}

}
