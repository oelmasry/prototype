package com.o.prototype.dao.basic.impl;

import org.springframework.stereotype.Repository;

import com.o.prototype.dao.basic.OIRBasicDAO;
import com.o.prototype.entities.OIR;

@Repository
public class OIRBasicDAOImpl extends BasicEntityDAOImpl implements OIRBasicDAO {

	public OIRBasicDAOImpl() {
		setType(OIR.class);
	}

}
