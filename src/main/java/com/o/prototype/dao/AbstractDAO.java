package com.o.prototype.dao;

import java.util.List;

public interface AbstractDAO<T> {

	public List<T> listAll();

	public T findById(Long id);

	public void create(T entity);

	public T update(T entity);

	public void deleteById(Long entityId);

	public List<T> batchUpdate(List<T> entities);

}