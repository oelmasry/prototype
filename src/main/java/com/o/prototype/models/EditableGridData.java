package com.o.prototype.models;

import java.util.Map;

public class EditableGridData {

	private Long id;

	private Map<String, Object> values;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, Object> getValues() {
		return values;
	}

	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

}
