package com.o.prototype.models;

public class EditableGridResponse {

	private int relationsUpdatedCount;

	public EditableGridResponse(int relationsUpdatedCount) {
		this.relationsUpdatedCount = relationsUpdatedCount;
	}

	public int getRelationsUpdatedCount() {
		return relationsUpdatedCount;
	}

	public void setRelationsUpdatedCount(int relationsUpdatedCount) {
		this.relationsUpdatedCount = relationsUpdatedCount;
	}

}
