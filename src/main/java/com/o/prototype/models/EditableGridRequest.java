package com.o.prototype.models;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * This class used when the submitted RequestBody is not hard - structured. This means the object has dynamic properties, e.g. Titelstruktur active Thema columns.
 * <br>The object properties are stored in a map, where the key is the property name, and the value is the property value.
 */
public class EditableGridRequest {

  private Map<String, Object> data;

  public EditableGridRequest() {
    data = new HashMap<>();
  }

  public Map<String, Object> getData() {
    return data;
  }

  @JsonAnySetter
  public void handleUnknown(String key, Object value) {
    data.put(key, value);
  }
}