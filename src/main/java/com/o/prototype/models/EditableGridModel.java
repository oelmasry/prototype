package com.o.prototype.models;

import java.util.List;
import java.util.Map;

public class EditableGridModel {

	private Map<String, String> dynamicColumns;

	private List<EditableGridData> data;

	public Map<String, String> getDynamicColumns() {
		return dynamicColumns;
	}

	public void setDynamicColumns(Map<String, String> dynamicColumns) {
		this.dynamicColumns = dynamicColumns;
	}

	public List<EditableGridData> getData() {
		return data;
	}

	public void setData(List<EditableGridData> data) {
		this.data = data;
	}


}
